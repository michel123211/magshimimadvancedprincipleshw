#include "BSNode.h"



BSNode::BSNode(string data)
{
	this->_left = NULL;
	this->_right = NULL;
	this->_count = 1;
	this->_data = data;
}

BSNode::BSNode(const BSNode& other)
{
	this->getData() = other.getData();
}

BSNode::~BSNode()
{}

BSNode& BSNode::operator=(const BSNode& other)
{
	if (this->_data == "")
	{
		this->_data = other.getData();
	}
	if (other.getRight() != NULL)
	{
		this->_right = new BSNode (other.getRight()->getData());
		*this->_right = *other.getRight();
	}
	if (other.getLeft() != NULL)
	{
		this->_left = new BSNode (other.getLeft()->getData());
		*this->_left = *other.getLeft();
	}

	return *this;
}

string BSNode::getData()const
{
	return _data;
}

BSNode* BSNode::getLeft() const
{
	return _left;
}

BSNode* BSNode::getRight() const
{
	return _right;
}

int BSNode::getCount() const
{
	return _count;
}
bool BSNode::search(string val)const
{
	if (!this)
	{	
		return false;
	}
	else if ((this->_left && this->_left->_data == val) || (this->_right && this->_right->_data == val) || this->_data.compare(val) == 0)
	{
		return true;
	}
	if (this->getRight()->getData() > val)
	{
		return (this->_left->search(val));
	}
	else
	{
		return (this->_right->search(val));
	}
}

void BSNode::insert(string value)
{
	BSNode* curr = this;
	if (!curr)
	{
		curr = new BSNode(value);
	}
	else if (!curr->_left && (curr->_data.compare(value) > 0))
	{
		curr->_left = new BSNode(value);
	}
	else if (!curr->_right && (this->_data.compare(value) < 0))
	{
		curr->_right = new BSNode(value);
	}
	else if (curr->_data.compare(value) > 0)
	{
		curr->_left->insert(value);
	}
	else if (curr->_data.compare(value) < 0)
	{
		curr->_right->insert(value);
	}
	else if (curr->_data.compare(value) == 0)
	{
		++this->_count;
	}
}

bool BSNode::isLeaf()const
{
	if (this->_right == NULL && this->_left == NULL)
	{
		return true;
	}
	return false;
}

int BSNode::getHeight() const
{
	if (this == NULL)
	{
		return 0;
	}
	else
	{
		int leftDepth = this->_left->getHeight();
		int rightDepth = this->_right->getHeight();
		if (leftDepth > rightDepth)
		{
			return(leftDepth + 1);
		}
		else {
			return(rightDepth + 1);
		}
	}
}
int BSNode::getDepth(const BSNode& root) const
{
	if (!root.search(this->_data))
		return -1;

	else if (root.getData() == this->_data)
		return 0;

	return max(this->getDepth(*root.getLeft()), this->getDepth(*root.getRight())) + 1;
}
