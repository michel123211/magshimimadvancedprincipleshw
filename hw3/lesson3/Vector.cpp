#include "Vector.h"
#include <string>
#include <iostream> 

using namespace std;


Vector::Vector(int n) :_size(n)
{
	if (n < 2)
	{
		n = 2;
	}
	this->_elements = new int[n];
	for (int i = 0; i < n; i++)
	{
		this->_elements[i] = EMPTY_ELEMENT;
	}
}
Vector::~Vector()
{
	delete[] _elements;
	_elements = nullptr;
}
int Vector::size() const
{
	return this->_size;
}
int Vector::capacity() const
{
	return this->_capacity;
}
int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}
bool Vector::empty()const
{
	if (this->_size == 0)
	{
		return true;
	}
	else {
		return false;
	}
}
void Vector::push_back(const int& val)
{
	if (this->_size < this->_capacity)
	{
		this->_elements[this->_size] = val;
		this->_size++;
		
	}
	else if (this->_size == this->_capacity)
	{
		// after we make reverse func
	}
}
int Vector::pop_back()
{
	if (this->_size > 0)
	{
		int removed_value;
		removed_value = this->_elements[this->_size];
		this->_size--;
		return removed_value;
	}
	else
	{
		cout << "Error: pop from empty vector" << endl;
		return -9999;
	}
}
void Vector::reserve(int n)
{
	if (n > this->_capacity)
	{
		while (this->_capacity < n)
		{
			this->_capacity += this->_resizeFactor;
		}		
	}
}
void Vector::resize(int n)
{
	int* temp = NULL;
	if (n > this->_capacity)
	{
		Vector::reserve(n);
	}
	if (n <= this->_size)
	{
		for (int i = 0; i < n; i++)
		{
			temp[i] = this->_elements[i];
		}
	}
	else if (n > this->_size)
	{
		for (int i = 0; i < this->_size; i++)
		{
			temp[i] = this->_elements[i];
		}
		for (int i = this->_size; i < n; i++)
		{
			temp[i] = EMPTY_ELEMENT;
		}
	}
	this->_elements = new int[n];
	for (int i = 0; i < n; i++)
	{
		this->_elements[i] = temp[i];
	}
}
void Vector::resize(int n, const int& val)
{
	int* temp = NULL;
	if (n > this->_capacity)
	{
		Vector::reserve(n);
	}
	if (n <= this->_size)
	{
		for (int i = 0; i < n; i++)
		{
			temp[i] = this->_elements[i];
		}
	}
	else if (n > this->_size)
	{
		for (int i = 0; i < this->_size; i++)
		{
			temp[i] = this->_elements[i];
		}
		for (int i = this->_size; i < n; i++)
		{
			temp[i] = val; // the only diffrence between the first resize function and this one.
		}
	}
	this->_elements = new int[n];
	for (int i = 0; i < n; i++)
	{
		this->_elements[i] = temp[i];
	}
}
void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}
Vector& Vector::operator=(const Vector& other)
{
	if (this == &other)
	{
		return *this;
	}
	delete[] this->_elements;
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;
	for (int i = 0; i < other._size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
	return *this;
}
Vector::Vector(const Vector& other)
{
	*this = other;
}

int& Vector::operator[](int n) const
{
	if (n > this->_size)
	{
		cout << "Index out of bounds" << endl;
		return this->_elements[0];
	}
	return this->_elements[n];
}