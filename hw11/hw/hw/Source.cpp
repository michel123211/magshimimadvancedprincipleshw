#include "Header.h"
#include <time.h>

using namespace std;

void I_Love_Threads()
{
	cout << "I love threads" << endl;
}

void call_I_Love_Threads()
{
	thread t1(I_Love_Threads);
	t1.join();
}


void getPrimes(int begin, int end, vector<int>& primes)
{
	bool checkPrime;
	for (; begin < end; begin++)
	{
		checkPrime = true;
		for (int i = 2; i < begin; i++)
		{
			if (begin % i == 0)
				checkPrime = false;
		}
		if (checkPrime)
		{
			primes.push_back(begin);
		}
	}
}
void printVector(vector<int> primes)
{
	for (int i = 0; i < primes.size(); ++i)
		cout << primes[i] << endl;
	cout << endl;
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes2;
	thread t1(getPrimes,begin,end,ref(primes2));
	t1.join();
	return primes2;
}

void writePrimesToFile(int begin, int end, ofstream& file)
{
	bool checkPrime;
	for (; begin < end; begin++)
	{
		checkPrime = true;
		for (int i = 2; i < begin; i++)
		{
			if (begin % i == 0)
				checkPrime = false;
		}
		if (checkPrime)
		{
			file << begin << endl;
		}
	}
}


void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	clock_t startTime;
	clock_t endTime;
	vector<thread> threadsVec;
	ofstream myfile;
	myfile.open(filePath);

	if (myfile.is_open())
	{
		int sendAmount = (end - begin) / N;
		int beginSend = 0;
		int endSend = begin;

		if ((end - begin) % N != 0)
		{
			sendAmount++;
		}

		for (int i = 0; i < sendAmount; i++)
		{
			if (i == 0)
			{
				beginSend = begin;
			}
			else
			{
				beginSend = (endSend + 1);
			}
			if (i == (sendAmount - 1))
			{
				endSend = end;
			}
			else
			{
				endSend = beginSend + (1 * N) - 1;
			}
			threadsVec.push_back(thread(writePrimesToFile, beginSend, endSend, ref(myfile)));
		}

		startTime = clock();
		for (int i = 0; i < threadsVec.size(); i++)
		{
			threadsVec[i].join();
		}
		endTime = clock();
		double duration_sec = double(endTime - startTime) / CLOCKS_PER_SEC;
		cout << "The time is" << duration_sec << endl;

		myfile.close();
	}
	else
	{
		cout << "The file didn't opened" << endl;
	}
}

int main()
{
	// �
	call_I_Love_Threads();

	// �
	vector<int> primes1;
	getPrimes(58, 100, primes1);
	printVector(primes1);

	vector<int> primes3;
	primes3 = callGetPrimes(93, 289);
	printVector(primes3);

	// �
	primes3 = callGetPrimes(0, 1000);
	printVector(primes3);

	primes3 = callGetPrimes(0, 100000);
	printVector(primes3);

	primes3 = callGetPrimes(0, 1000000);
	printVector(primes3);

	// �
	callWritePrimesMultipleThreads(1, 100000, "primes2.txt", 2);

	// �
	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 2);
	callWritePrimesMultipleThreads(1, 100000, "primes2.txt", 2);
	callWritePrimesMultipleThreads(1, 1000000, "primes2.txt", 2);


	system("pause");
	return 0;
}