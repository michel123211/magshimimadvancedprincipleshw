#include "OutStream.h"
#include <stdio.h>
#include <string>
#include <iostream>

OutStream::OutStream()
{
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	printf("%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	printf("%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}

void endline()
{
	printf("\n");
}

//OutStreamEncrypted::OutStreamEncrypted(int size, std::string str) : encryption_size(size), encryption_string(str)
//{
//}
//OutStreamEncrypted::~OutStreamEncrypted()
//{
//}
//std::string OutStreamEncrypted::get_encryption(std::string str)
//{
//	for (int i = 0; i < str.length; i++)
//	{
//		str[i] = str[i] + 3;
//	}
//	this->encryption_string = str;
//	return this->encryption_string;
//}