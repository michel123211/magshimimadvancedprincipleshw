#pragma once
#include <string>
#include <iostream>
class OutStream
{
public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)());
};

void endline();

class OutStreamEncrypted : public OutStream
{
private:
	int encryption_size;
	std::string encryption_string;

public:
	OutStreamEncrypted(int size, std::string str);
	~OutStreamEncrypted();
	std::string get_encryption(const std::string newEncryptionString);

};