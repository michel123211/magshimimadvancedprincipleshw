#pragma once

class OutStream
{
protected:
	FILE* file;
public:
	OutStream();
	~OutStream();
	
	OutStream& operator<<(const char* str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)());
};

void endline();

class Filestream : public OutStream
{
public:
	Filestream(char* n);
	~Filestream();
		
private:
	char* path;

};