#include "Item.h"

Item::Item(string name, string serialNumber, double unitPrice)
	:
	_name(name),
	_serialNumber(serialNumber),
	_unitPrice(unitPrice)
{
	this->_count = 1;
}
Item::~Item()
{}

double Item::totalPrice() const
{
	return 0;
}

bool Item::operator<(const Item& other) const
{
	return (this->getSerialNumber() < other.getSerialNumber());
}
bool Item::operator>(const Item& other) const
{
	return (this->getSerialNumber() > other.getSerialNumber());
}
bool Item::operator==(const Item& other) const
{
	return (this->getSerialNumber() == other.getSerialNumber());
}

//getters

string Item::getName() const
{
	return this->_name;
}

string Item::getSerialNumber() const
{
	return this->_serialNumber;
}
double Item::getUnitPrice() const
{
	return this->_unitPrice;
}

int Item::getCount() const
{
	return this->_count;
}

// setters

void Item::setCount(const int c)
{
	this->_count = c;
}