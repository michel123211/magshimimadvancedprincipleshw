#pragma once
#include "Item.h"
#include <set>
#include <map>

class Customer
{
public:
	Customer(string);
	Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item);//add item to the set
	void removeItem(Item);//remove item from the set

	string getName() const; 
	set<Item> getItems() const;
	//get and set functions

private:
	string _name;
	set<Item> _items;
};