#include"Customer.h"
#include"Item.h"
#include<map>
#include <iostream>

#define ADD_ITEM_DEFAULT 1

using namespace std;

Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };

void updateCustomer(map<string, Customer>& abcCustomers);
void showMenu();
void makeCustomer(map<string, Customer>& abcCustomers);
void addOrRemoveItem(string CustomerName, map<string, Customer>& abcCustomers, int type);
void printCustomerList(set<Item> customerItems);
void printBiggestPayer(map<string, Customer>& abcCustomers);
bool checkIfExist(Item item, set<Item> customerItems);
void highestPayer(map<string, Customer>& abcCustomers);



void highestPayer(map<string, Customer>& abcCustomers)
{
	int highestPayment = 0;
	map<string, Customer>::iterator it = abcCustomers.begin();
	pair<string, Customer> highestPayerCustomer;

	if (abcCustomers.size() == 0)
	{
		cout << "No customers" << endl << endl;
	}
	else
	{
		for (; it != abcCustomers.end(); it++)
		{
			if (highestPayment < (*it).second.totalSum() && !((*it).second.getItems().empty()))
			{
				highestPayment = (*it).second.totalSum();
				highestPayerCustomer = (*it);
			}
		}
		if (highestPayerCustomer.first == "")
		{
			cout << "Customer Not found!" << endl << endl;
		}
		else
		{
			cout << "Name: " << highestPayerCustomer.first << " Price: " << highestPayerCustomer.second.totalSum() << endl << "Items list: ";
		}
		printCustomerList(highestPayerCustomer.second.getItems());
	}

}





bool checkIfExist(Item item, set<Item> customerItems)
{
	set<Item>::iterator it;

	for (it = customerItems.begin(); it != customerItems.end(); it++)
	{
		if ((*it) == item)
		{
			return true;
		}
	}
	return false;
}

void addOrRemoveItem(string CustomerName, map<string, Customer>& abcCustomers, int type)
{
	int buyItemChoice = -1;

	while (buyItemChoice != 0)
	{
		cout << "The items you can buy or remove are : (0 to exit)" << endl << endl;

		for (int i = 0; i < 10; i++)
			cout << i + 1 << "." << itemList[i].getName() << " Price: " << itemList[i].getUnitPrice() << endl;

		cin >> buyItemChoice;
		
		if (buyItemChoice < 0 || buyItemChoice > 10)
		{
			cout << "Worng input" << endl << endl;
		}

		else if (buyItemChoice != 0 && type == 1)
		{
			abcCustomers[CustomerName].addItem(itemList[buyItemChoice - 1]);
		}
		else if (buyItemChoice != 0 && type == 2)
		{
			if (checkIfExist(itemList[buyItemChoice-1], abcCustomers[CustomerName].getItems()))
			{
				if (abcCustomers[CustomerName].getItems().find(itemList[buyItemChoice - 1])->getCount() != 0)
				{
					abcCustomers[CustomerName].removeItem(itemList[buyItemChoice - 1]);
					printCustomerList(abcCustomers[CustomerName].getItems());
				}
			}
			else
			{
				cout << "You don't have this item in your cart" << endl << endl;
			}
			
		}
	}
}

void printCustomerList(set<Item> customerItems)
{
	set<Item>::iterator it = customerItems.begin();

	if (it != customerItems.end())
	{
		cout << "This are the items in your cart now" << endl << endl;

		for (; it != customerItems.end(); ++it)
		{
			cout << "Name: " << (it)->getName() << " Price: " << (it)->getUnitPrice() << " Count: " << (it)->getCount() << endl;
		}
	}
	else
	{
		cout << "Your cart is empty" << endl << endl;
	}
	cout << endl;
}

void showMenu()
{
	cout << "Welcome to MagshiMart!" << endl;
	cout << "1. To sign as customer and buy items" << endl;
	cout << "2. To uptade existing customer's items" << endl;
	cout << "3. To print the customer who pays the most" << endl;
	cout << "4. To exit" << endl;
}

void makeCustomer(map<string, Customer>& abcCustomers)
{
	string CustomerName;
	map<string, Customer>::iterator it;

	cout << "Please enter your name: ";
	cin >> CustomerName;
	cout << endl;

	it = abcCustomers.find(CustomerName);
	if (it != abcCustomers.end())
	{
		cout << "This name exist" << endl << endl;
	}
	else
	{
		addOrRemoveItem(CustomerName, abcCustomers, ADD_ITEM_DEFAULT);
	}
}

void updateCustomer(map<string, Customer>& abcCustomers)
{
	string CustomerName;
	int addOrRemoveChoice = -1;

	cout << "Please identify by entering your name: ";
	cin >> CustomerName;
	cout << endl;

	if (abcCustomers.find(CustomerName) == abcCustomers.end())
	{
		cout << "No such a name :(" << endl << endl;
	}
	else
	{
		while (addOrRemoveChoice != 3)
		{
			cout << "1. Add items" << endl;
			cout << "2. Remove items" << endl;
			cout << "3. Back to menu" << endl;

			cin >> addOrRemoveChoice;

			if (addOrRemoveChoice == 1)
			{
				addOrRemoveItem(CustomerName, abcCustomers, addOrRemoveChoice);
			}
			else if (addOrRemoveChoice == 2)
			{
				addOrRemoveItem(CustomerName, abcCustomers, addOrRemoveChoice);
			}
			else if (addOrRemoveChoice == 3)
			{
				cout << "Going back to menu" << endl << endl;
			}
			else
			{
				cout << "Worng input :/" << endl << endl;
			}
		}
	}
}

int main()
{
	int choice = 0;
	int addOrRemoveChoice = -1;
	map<string, Customer> abcCustomers;

	while (choice != 4)
	{
		showMenu();
		cin >> choice;

		switch (choice)
		{
		case 1:
			makeCustomer(abcCustomers);
			break;
		case 2:
			updateCustomer(abcCustomers);
			break;
		case 3:
			highestPayer(abcCustomers);
			break;
		case 4:
			cout << "GoodBye! :)" << endl;
			break;
		default:
			cout << "Worng input :/" << endl << endl;
			break;
		}
	}
	return 0;
}