#include "Customer.h"

Customer::Customer(string data)
	:
	_name(data)
{}
Customer::Customer()
{}
double Customer::totalSum() const
{
	double totalPrice = 0;
	set<Item>::iterator it;

	for (it = this->_items.begin(); it != this->_items.end(); it++)
	{
		totalPrice = it->getUnitPrice()* it->getCount();
	}
	return totalPrice;
}
void Customer::addItem(Item item)
{
	set<Item>::iterator itemIt;
	bool found = false;

	for (itemIt = this->_items.begin(); (itemIt != this->_items.end() && !found); itemIt++)
	{
		if ((*itemIt) == item)
		{
			found = true;
			item.setCount((*itemIt).getCount() + 1);
		}
	}
	if (found)
	{
		this->_items.erase(item);
	}
	this->_items.insert(item);
}

void Customer::removeItem(Item item)
{
	set<Item>::iterator itemIt;
	bool found = false;

	for (itemIt = this->_items.begin(); (itemIt != this->_items.end() && !found); itemIt++)
	{
		if ((*itemIt) == item)
		{
			found = true;
			item.setCount((*itemIt).getCount() - 1);
		}
	}

	if (item.getCount() == 0 && found)
	{
		this->_items.erase(item);
	}
	else if (found)
	{
		this->_items.erase(item);
		this->_items.insert(item);
	}

}

string Customer::getName() const
{
	return this->_name;
}
set<Item> Customer::getItems() const
{
	return this->_items;
}