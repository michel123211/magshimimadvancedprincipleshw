#include <iostream>
#include "shape.h"
#include "MathUtils.h"



class Hexagon : public Shape
{
public:
	Hexagon(string nam, string col, double ang);
	virtual void draw();
	virtual double CalArea(); 
	double CalCircumference();

	double getAng();
	void setAng(const double ang);
private:
	double ang;
};