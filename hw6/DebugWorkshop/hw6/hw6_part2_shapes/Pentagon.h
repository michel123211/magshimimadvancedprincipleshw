#include <iostream>
#include "shape.h"
#include "MathUtils.h"


class Pentagon : public Shape
{
private: 
	double ang;

public:
	Pentagon(string nam, string col, double ang);
	virtual void draw(); 
	virtual double CalArea();
	double CalCircumference();

	double getAng();
	void setAng(const double ang);
};