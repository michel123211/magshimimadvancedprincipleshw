#include "MathUtils.h"

MathUtils::MathUtils()
{
}

double MathUtils::CalPentagonArea(double ang)
{
	double result = (5 * pow(ang, 2) * sqrt(3)) / 4;
	return result;
}

double MathUtils::CalHexagonArea(double ang)
{
	double result = (6 *pow(ang, 2) * sqrt(3)) / 4;
	return result;
}