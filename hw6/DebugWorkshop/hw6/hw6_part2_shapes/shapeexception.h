#pragma once
#include <exception>

class shapeException : public exception
{
	virtual const char* what() const
	{
		return "This is a shape exception!\n";
	}
};