#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "InputExeption.h"
#include "Hexagon.h"
#include "Pentagon.h"

#include <typeinfo>

using namespace std;

#define BUFFER_SUCCESS 10

int main()
{
	string nam, col;
	double rad = 0, ang = 0, ang2 = 180;
	int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon tagon(nam, col, ang);
	Hexagon xagon(nam, col, ang);

	Shape* ptrcirc = &circ;
	Shape* ptrquad = &quad;
	Shape* ptrrec = &rec;
	Shape* ptrpara = &para;
	Shape* ptrtagon = &tagon;
	Shape* ptrxagon = &xagon;



	cout << "Enter information for your objects" << endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		try
		{
			cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h = Hexagon, g = pentagon" << endl;
			cin >> shapetype;

			if (cin.peek() != BUFFER_SUCCESS)
			{
				cin.clear();
				cin.ignore(numeric_limits<streamsize>::max(), '\n');
				throw string("Warning - Don't try to build more than one shape at once");
			}
		}

		catch (string & error)
		{
			cout << error << endl;
		}


		try
		{

			switch (shapetype) {
			case 'c':
				cout << "enter color, name,  rad for circle" << endl;
				cin >> col >> nam >> rad;
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				cout << "enter name, color, height, width" << endl;
				cin >> nam >> col >> height >> width;
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				cout << "enter name, color, height, width" << endl;
				cin >> nam >> col >> height >> width;
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				cout << "enter name, color, height, width, 2 angles" << endl;
				cin >> nam >> col >> height >> width >> ang >> ang2;
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case 'h':
				cout << "enter name, color, 1 angles" << endl;
				cin >> nam >> col >> ang;
				xagon.setName(nam);
				xagon.setColor(col);
				xagon.setAng(ang);
				ptrxagon->draw();
				break;
			case 'g':
				cout << "enter name, color, 1 angles" << endl;
				cin >> nam >> col >> ang;
				tagon.setName(nam);
				tagon.setColor(col);
				tagon.setAng(ang);
				ptrtagon->draw();
				break;


			default:
				cout << "you have entered an invalid letter, please re-enter" << endl;
				break;
			}
			cout << "would you like to add more object press any key if not press x" << endl;
			cin >> x;
		}

		catch (exception & e)
		{
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}

	system("pause");
	return 0;
}