#include <iostream>
#include <math.h>

#pragma once

class MathUtils
{
public:
	MathUtils();
	static double CalPentagonArea(const double ang);
	static double CalHexagonArea(const double ang);
};