#include "shape.h"
#include "rectangle.h"
#include "quadrilateral.h"
#include "shapeException.h"
#include <iostream>

using namespace std;

rectangle::rectangle(string nam, string col, int w, int h)
	:quadrilateral(nam, col, w, h) 
{}

void rectangle::draw()
{
	cout << getName() << endl << getColor() << endl << "Height is " << getHeight() << endl << "Width is " << getWidth() << endl
		<< "area is " << CalArea(getWidth(), getHeight()) << endl << "is square (1,0)?: " << isSquare(getWidth(), getHeight()) << endl;
}

double rectangle::CalArea(double w, double h)
{
	if (w < 0 || h < 0)
	{
		throw shapeException();
	}
	return w * h;
}

bool rectangle::isSquare(int w, int h)
{
	if (w == h)
	{
		return true;
	}
	else
	{
		return 0;
	}
}
