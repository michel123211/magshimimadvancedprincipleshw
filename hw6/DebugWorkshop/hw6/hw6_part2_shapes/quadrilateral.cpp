#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "shapeException.h"
#include "InputExeption.h"
#include <iostream>

using namespace std;

quadrilateral::quadrilateral(string nam, string col, int h, int w) 
	:Shape(nam, col)
{
	setHeight(h);
	setWidth(w);
}
void quadrilateral::draw()
{
	cout << getName() << endl << getColor() << endl << "Width is " << getWidth() << endl << "Height is " << getHeight() << endl << "Area is " << CalArea() << endl << "Perimeter is " << getCalPerimater() << endl;
}

double quadrilateral::CalArea()
{
	if (width < 0 || height < 0)
	{
		throw shapeException();
	}
	return width * height; 
}

void quadrilateral::setHeight(int h)
{
	if (cin.fail())
	{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		throw InputExeption();
	}
	else if (h < 0)
	{
		throw shapeException();
	}
	height = h;
}
void quadrilateral::setWidth(int w)
 {
	if (cin.fail())
	{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		throw InputExeption();
	}

	else if (w < 0)
	{
		throw shapeException();
	}
	width = w;
}
double quadrilateral::CalPerimater() 
{
	return 2 * (height + width);
}
double quadrilateral::getCalPerimater() 
{
	return 2 * (height + width);
}
int quadrilateral::getHeight()
{
	return height;
}
int quadrilateral::getWidth()
{
	return width;
}