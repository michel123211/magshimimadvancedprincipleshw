#include <iostream>

using namespace std;

#define BUG -1
#define SUCCESS 1
#define ERROR_MSG "This user is not authorized to access 8200, please enter different numbers, or try to get .clearance in 1 year"

int add(int a, int b);
int multiply(int a, int b);
int pow(int a, int b);
void error_func();
void success_func();


int add(int a, int b)
{
	if ((a == 8200) || (b == 8200) || ((a + b) == 8200))
	{
		return BUG;
	}
	return a + b;
}

int multiply(int a, int b)
{
	int sum = 0;
	if ((a == 8200) || (b == 8200))
	{
		return BUG;
	}

	for (int i = 0; i < b; i++)
	{
		sum = add(sum, a);
		if (sum == 8200)
		{
			return BUG;
		}
	}
	return sum;
}

int pow(int a, int b)
{
	if ((a == 8200) || (b == 8200))
	{
		return BUG;
	}

	else
	{
		int temp = 1;
		for (int i = 0; i < b; i++)
		{
			temp = multiply(temp, a);

			if (temp == 8200)
			{
				return BUG;
			}
		}
		return temp;
	}
}

void error_func()
{
	int result_add = add(8000, 200);


	if (BUG == result_add)
	{
		cout << ERROR_MSG << endl;
	}
	else
	{
		cout << result_add << endl;
	}

	int result_multiply = multiply(100, 82);

	if (BUG == result_multiply) 
	{
		cout << ERROR_MSG << endl;
	}
	else
	{
		cout << result_multiply << endl;
	}


	int result_pow = pow(8200, 5);

	if (BUG == result_pow)
	{
		cout << ERROR_MSG << endl;
	}
	else
	{
		cout << result_pow << endl;
	}
}


void success_func()
{

	int result_add = add(5, 25);

	if (BUG == result_add)
	{
		cout << ERROR_MSG << endl;
	}
	else
	{
		cout << result_add << endl;
	}


	int result_multiply = multiply(80, 2);

	if (BUG == result_multiply)
	{
		cout << ERROR_MSG << endl;
	}
	else
	{
		cout << result_multiply << endl;
	}


	int result_pow = pow(80, 2);

	if (BUG == result_pow)
	{
		cout << ERROR_MSG << endl;
	}
	else
	{
		cout << result_pow << endl;
	}
}

void main() 
{
	error_func();
	cout << "\n\n" << endl;
	success_func();
}