#include <iostream>

using namespace std;

#define ERROR_MSG "This user is not authorized to access 8200, please enter different numbers, or try to get .clearance in 1 year"

void error_func();
void success_func();

int add(int a, int b)
{
	if (((a == 8200) || (b == 8200)) || ((a + b) == 8200))
	{
		throw string(ERROR_MSG);
	}
	return a + b;
}

int  multiply(int a, int b)
{
	try
	{
		int sum = 0;
		for (int i = 0; i < b; i++)
		{
			try
			{
				sum = add(sum, a);
			}

			catch (string & error)
			{
				throw string(ERROR_MSG);
			}
		}

		if (sum == 8200)
		{
			throw string(ERROR_MSG);
		}
		return sum;
	}

	catch (string & error)
	{
		throw string(ERROR_MSG);
	}
}

int  pow(int a, int b)
{
	try
	{
		int temp = 1;
		for (int i = 0; i < b; i++)
		{
			try
			{
				temp = multiply(temp, a);
			}

			catch (string & error)
			{
				throw string(ERROR_MSG);
			}

		}
		return temp;
	}

	catch (string & error)
	{
		throw string(ERROR_MSG);
	}
}

void error_func()
{
	try
	{
		cout << pow(4100, 2) << endl;
	}

	catch (string & error)
	{
		cout << error << endl;
	}

	try
	{
		cout << multiply(4100, 2) << endl;
	}

	catch (string & error)
	{
		cout << error << endl;
	}

	try
	{
		cout << add(1, 8199) << endl;
	}

	catch (string & error)
	{
		cout << error << endl;
	}
}


void success_func()
{
	try
	{
		cout << pow(2, 4) << endl;
	}

	catch (string & error)
	{
		cout << error << endl;
	}

	try
	{
		cout << multiply(4, 10) << endl;
	}

	catch (string & error)
	{
		cout << error << endl;
	}

	try
	{
		cout << add(5, 20) << endl;
	}

	catch (string & error)
	{
		cout << error << endl;
	}
}
void main()
{
	error_func();
	cout << "\n\n\n" << endl;
	success_func();
}