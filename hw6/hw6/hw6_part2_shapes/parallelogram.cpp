
#include "parallelogram.h"
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "shapeException.h"
#include "InputExeption.h"
#include <iostream>
using namespace std;

#define MAX_ANGLE 180


parallelogram::parallelogram(string col, string nam, int h, int w, double ang, double ang2)
	: quadrilateral(col, nam, h, w)
{
	setAngle(ang, ang2);
}

void parallelogram::draw()
{
	cout << getName() << endl << getColor() << endl << "Height is " << getHeight() << endl << "Width is " << getWidth() << endl
		<< "Angles are: " << getAngle() << "," << getAngle2() << endl << "Area is " << CalArea(getWidth(), getHeight()) << endl;
}

double parallelogram::CalArea(double w, double h)
{
	if (w < 0 || h < 0)
	{
		throw shapeException();
	}
	return w * h;
}
void parallelogram::setAngle(double ang, double ang2)
{
	if (std::cin.fail())
	{
		std::cin.clear();
		std::cin.ignore(numeric_limits<streamsize>::max(), '\n');
		throw InputExeption();
	}

	else if (ang < 0 || ang2 < 0)
	{
		throw shapeException();
	}
	else if (((ang < 0) || (ang > MAX_ANGLE)) || ((ang2 < 0) || (ang2 > MAX_ANGLE)))
	{
		throw shapeException();
	}

	angle = ang;
	angle2 = ang2;
}

double parallelogram::getAngle()
{
	return angle;
}
double parallelogram::getAngle2() 
{
	return angle2;
}
