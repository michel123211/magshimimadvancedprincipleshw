#include "Hexagon.h"
#include "shapeException.h"
#include "InputExeption.h"


Hexagon::Hexagon(string nam, string col, double ang)
	: Shape(nam, col)
{
	setAng(ang);
}

void Hexagon::draw()
{
	cout << endl << "Color is " << getColor() << endl << "Name is " << getName() << endl << "radius is " << CalArea() << endl << "Circumference: " << CalCircumference() << endl;
}

double Hexagon::CalCircumference()
{
	return 6 * this->ang;
}

double Hexagon::CalArea()
{
	MathUtils* culc = new MathUtils();
	return culc->CalHexagonArea(this->ang);
	delete culc;
}

void Hexagon::setAng(const double ang)
{
	if (cin.fail())
	{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		throw InputExeption();
	}
	this->ang = ang;
}

double Hexagon::getAng()
{
	return this->ang;
}