#include "Pentagon.h"
#include "shapeException.h"
#include "InputExeption.h"

using namespace std;

Pentagon::Pentagon(string nam, string col, double ang)
	: Shape(nam, col)
{
	setAng(ang);
}

void Pentagon::draw()
{
	cout << endl << "Color: " << getColor() << endl << "Name: " << getName() << endl << "Radius: " << CalArea() << endl << "Circumference: " << CalCircumference() << endl;
}

double Pentagon::CalCircumference()
{
	return 5 * this->ang;
}

double Pentagon::CalArea()
{
	MathUtils* culc = new MathUtils();
	return culc->CalPentagonArea(this->ang);
	delete culc;
}

void Pentagon::setAng(const double ang)
{
	if (cin.fail())
	{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		throw InputExeption();
	}

	if (ang < 0)
	{
		throw shapeException();
	}
	this->ang = ang;
}

double Pentagon::getAng()
{
	return this->ang;
}