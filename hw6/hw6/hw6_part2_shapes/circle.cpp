#include "shape.h"
#include "circle.h"
#include "shapeException.h"
#include "InputExeption.h"
#include <iostream>

using namespace std;

Circle::Circle(string nam, string col, double rad)
	:Shape(col, nam)
{
	setRad(rad);
}
void Circle::draw()
{
	cout << endl << "Color: " << getColor() << endl << "Name: " << getName() << endl << "Radius: " << getRad() << endl << "Circumference: " << CalCircumference() << endl;
}

void Circle::setRad(double rad)
{
	if (cin.fail())
	{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		throw InputExeption();
	}

	if (rad < 0) 
	{
		throw shapeException();
	}
	radius = rad;
}

double Circle::CalArea()
{
	double area = 3.14 * radius * radius;
	return area;
}

double Circle::getRad()
{
	return radius;
}


double Circle::CalCircumference()
{
	double circumference = 2 * (3.14) * radius;
	if (circumference < 0)
	{
		throw shapeException();
	}
	return circumference;
}