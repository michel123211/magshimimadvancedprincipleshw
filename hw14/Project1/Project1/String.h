#ifndef STRING_H
#define STRING_H

#include "Sequence.h"
#include <string>

using namespace std;

class String : public Sequence
{
public:
	String(string);
	~String();

	virtual bool isPrintable() const;
	virtual string toString() const;

private:
	string s;
};

#endif // STRING_H