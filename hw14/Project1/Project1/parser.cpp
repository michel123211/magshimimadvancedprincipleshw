#include "parser.h"
#include <iostream>
#include "IndentationException.h"
#include "SyntaxException.h"

using namespace std;

Type* Parser::parseString(std::string str) throw()
{
	Helper::rtrim(str);

	if (str[0] == ' ' || str[0] == '\t')
	{
		throw IndentationException();
	}
	else if (str.length() > 0)
	{
		if (getType(str) != NULL)
			return (getType(str));
		else
			throw SyntaxException();
	}

	return NULL;
}

Type* Parser::getType(string& str)
{
	Helper::trim(str);

	if (Helper::isInteger(str))
	{
		Integer* i = new Integer(stoi(str));
		i->setIsTemp(true);
		return i;
	}	
	else if (Helper::isBoolean(str))
	{
		if (str == "true")
		{
			Boolean* b = new Boolean(true);
			b->setIsTemp(true);
			return b;
		}
		else
		{
			Boolean* b = new Boolean(false);
			b->setIsTemp(true);
			return b;
		}
	}
	else if (Helper::isString(str))
	{
		String* s = new String(str);
		return s;
	}
	else
	{
		return NULL;
	}
}


