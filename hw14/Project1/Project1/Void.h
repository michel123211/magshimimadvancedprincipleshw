#ifndef VOID_H
#define VOID_H

#include "type.h"
#include <string>

using namespace std;

class Void : public Type
{
public:
	Void();
	~Void();

	virtual bool isPrintable() const;
	virtual string toString() const;

private:
	
};

#endif // VOID_H