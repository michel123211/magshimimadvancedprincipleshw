#include "Integer.h"

Integer::Integer(int n) : Type()
{
	this->num = n;
}

Integer::~Integer()
{}

bool Integer::isPrintable() const
{
	return true;
}

string Integer::toString() const
{
	return to_string(this->num);
}