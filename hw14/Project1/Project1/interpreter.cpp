#include "type.h"
#include "InterperterException.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include "parser.h"
#include <iostream>

using namespace std;

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "MICHEL NECHAEV"


int main(int argc,char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()")
	{
		// prasing command
		Type* t = nullptr;
		try
		{
			t = Parser::parseString(input_string);

			if (t->isPrintable())
			{
				cout << t->toString() << endl;
			}

			if (t->getIsTemp())
			{
				delete(t);
			}
		}
		catch (const IndentationException &e)
		{
			cout << e.what() << endl;
		}
		catch (const SyntaxException & e)
		{
			cout << e.what() << endl;
		}

		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	return 0;
}


