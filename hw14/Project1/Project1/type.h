#ifndef TYPE_H
#define TYPE_H

#include <string>
#include <iostream>

using namespace std;

class Type
{
public:
	Type();
	virtual ~Type();

	bool getIsTemp() const;
	void setIsTemp(const bool);


	virtual bool isPrintable() const = 0;
	virtual string toString() const = 0;

private:
	bool _isTemp = false;
};
#endif //TYPE_H
