#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"
#include <string>

using namespace std;

class Integer : public Type
{
public:
	Integer(int);
	~Integer();

	virtual bool isPrintable() const;
	virtual string toString() const;
private:
	int num;
};

#endif // INTEGER_H