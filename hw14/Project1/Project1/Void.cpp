#include "Void.h"

Void::Void() : Type()
{}

Void::~Void()
{}

bool Void::isPrintable() const
{
	return false;
}

string Void::toString() const
{
	return NULL;
}