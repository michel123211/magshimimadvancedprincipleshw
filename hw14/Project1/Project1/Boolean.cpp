#include "Boolean.h"

Boolean::Boolean(bool bol) : Type()
{
	this->bol = bol;
}

Boolean::~Boolean()
{}

bool Boolean::isPrintable() const
{
	return true;
}

string Boolean::toString() const
{
	return this->bol ? "true" : "false";
}