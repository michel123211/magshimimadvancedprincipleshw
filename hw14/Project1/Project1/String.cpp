#include "String.h"

String::String(string s) : Sequence()
{
	this->s = s;
}

String::~String()
{}

bool String::isPrintable() const
{
	return true;
}

string String::toString() const
{
	return this->s;
}