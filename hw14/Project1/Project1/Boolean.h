#ifndef BOOLEAN_H
#define BOOLEAN_H

#include "type.h"
#include <string>

using namespace std;

class Boolean : public Type
{
public:
	Boolean(bool);
	~Boolean();

	virtual bool isPrintable() const;
	virtual string toString() const;

private:
	bool bol;
};

#endif // BOOLEAN_H