#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <vector>
#include <thread>
#include <string>


using namespace std;

class Server
{
public:
	Server();
	~Server();
	void serve(int port);
	

private:
	vector<thread> vecOfThreads;
	void accept();
	void clientHandler(SOCKET clientSocket);

	SOCKET _serverSocket;
};
