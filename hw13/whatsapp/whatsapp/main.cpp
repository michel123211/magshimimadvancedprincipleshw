#pragma comment (lib, "ws2_32.lib")

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib> 
#include "Server.h"

using namespace std;

#define TWO_FIRST_LINES 2

void findPortOfServer(string str[TWO_FIRST_LINES]);


void findPortOfServer(string str[TWO_FIRST_LINES])
{
	ifstream file("config.txt");
	
	for (int i = 0; i < TWO_FIRST_LINES; i++)
	{
		getline(file, str[i]);
	}

	for (int i = 0; i < TWO_FIRST_LINES; i++)
	{
		size_t pos = str[i].find('=');
		str[i] = str[i].substr(pos + 1);
	}
}

int main()
{

	string str[TWO_FIRST_LINES];
	findPortOfServer(str);
	cout << str[0] << endl << str[1];
	int port = stoi(str[1]);
	Server serv;
	serv.serve(port);
	
}